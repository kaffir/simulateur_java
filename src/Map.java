import java.util.ArrayList;
import java.util.List;

public class Map {
	private float latitude;
	private float longitude;
	private float latitudeSpan;
	private float longitudeSpan;
	private List<MapItem> mapItems = new ArrayList<MapItem>();
	
	public Map(float lat, float lon, float latSpan, float lonSpan) {
		latitude = lat;
		longitude = lon;
		latitudeSpan = latSpan;
		longitudeSpan = lonSpan;
	}
	
	public void addItem(MapItem i) {
		mapItems.add(i);
	}
	
	public List<MapItem> compareWithMapItems(float lat, float lon) {
		List<MapItem> response = new ArrayList<MapItem>();
		for(MapItem current : mapItems) {
			float itemLat = current.getLatitude();
			float itemLong = current.getLongitude();
			if(Math.sqrt(Math.pow((itemLat-lat), 2)+Math.pow((itemLong-lon),2)) <= current.getInfluence()) {
				response.add(current);
			}
		}
		return response;
	}
	
	public List<MapItem> getMapItems() {
		return mapItems;
	}
	public float getLatitude() {
		return latitude;
	}
	public float getLongitude() {
		return longitude;
	}
	public float getLatitudeSpan() {
		return latitudeSpan;
	}
	public float getLongitudeSpan() {
		return longitudeSpan;
	}
}
