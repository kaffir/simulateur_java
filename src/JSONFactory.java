import java.util.ArrayList;
import java.util.List;

import org.json.*;

public class JSONFactory {
	
	public List<Player> getPlayers(JSONObject json) {
		List<Player> playersList = new ArrayList<Player>();
		JSONObject map = json.getJSONObject("map");
		JSONArray players = map.getJSONArray("ranking");
		for(int i=0; i<players.length(); i++) {
			JSONObject playerInfo = map.getJSONObject("playerInfo").getJSONObject(players.getString(i));
			Player p = new Player(players.getString(i), 
					(float)playerInfo.getDouble("cash"), 
					((playerInfo.isNull("sales"))? 0 : playerInfo.getInt("sales")), 
					((playerInfo.isNull("profit"))? (float)0.0 : (float)playerInfo.getDouble("profit")));
			JSONArray drinks = playerInfo.getJSONArray("drinksOffered");
			for(int j=0; j<drinks.length(); j++) { 
				p.addRecipe(new Recipe(p, 
						drinks.getJSONObject(j).getString("name"), 
						(float)drinks.getJSONObject(j).getDouble("price"), 
						drinks.getJSONObject(j).getBoolean("hasAlcohol"), 
						drinks.getJSONObject(j).getBoolean("isCold")));
			}
			playersList.add(p);
		}
		return playersList;
	}
	
	public Map getMap(JSONObject json, List<Player> players) {
		JSONObject jsonMap = json.getJSONObject("map");
		float latitude = (float)jsonMap.getJSONObject("region").getJSONObject("center").getDouble("latitude");
		float longitude = (float)jsonMap.getJSONObject("region").getJSONObject("center").getDouble("longitude");
		float latitudeSpan = (float)jsonMap.getJSONObject("region").getJSONObject("span").getDouble("latitudeSpan");
		float longitudeSpan = (float)jsonMap.getJSONObject("region").getJSONObject("span").getDouble("longitudeSpan");
		Map map = new Map(latitude, longitude, latitudeSpan, longitudeSpan);
		for(int i=0; i<players.size(); i++) {
			JSONArray itemArray = jsonMap.getJSONObject("itemsByPlayer").getJSONArray(players.get(i).getName());
			for(int j=0; j<itemArray.length(); j++) {
				map.addItem(new MapItem(
						MapItemKind.valueOf(itemArray.getJSONObject(j).getString("kind")), 
						players.get(i), 
						(float)itemArray.getJSONObject(j).getDouble("influence"), 
						(float)itemArray.getJSONObject(j).getJSONObject("location").getDouble("latitude"),
						(float)itemArray.getJSONObject(j).getJSONObject("location").getDouble("longitude")));
			}
		}
		return map;
	}
	
	public long getTime(JSONObject json) {
		return json.getLong("timestamp");
	}
	
	public Weather getWeather(JSONObject json) {
		Weather w = Weather.sunny;
		JSONArray weather = json.getJSONArray("weather");
		for(int i=0; i<json.length(); i++) {
			JSONObject forecast = weather.getJSONObject(i);
			if(forecast.getInt("dfn") == 0) {
				w = Weather.valueOf(forecast.getString("weather"));
			}
		}
		return w;
	}
	
	public JSONObject makeSales(Recipe r) {
		JSONObject sale = new JSONObject();
		sale.put("player", r.getOwner().getName());
		sale.put("item", r.getName());
		sale.put("quantity", 1);
		return (new JSONObject()).put("sales", (new JSONArray()).put(sale));
	}
}
