import java.util.ArrayList;
import java.util.List;

public class Customer {
	private float latitude;
	private float longitude;
	private boolean hasDrinkToday;
	
	public Customer(float latMin, float latMax, float lonMin, float lonMax) {
		latitude = latMin + (float)Math.random() * (latMax - latMin);
		longitude = lonMin + (float)Math.random() * (lonMax - lonMin);
		hasDrinkToday = false;
	}
	
	private Recipe getNearest(List<Recipe> selection) {
		Recipe choice = null;
		for(int j=0; j<selection.size(); j++) {
			if(choice == null) {
				choice = selection.get(j);
			}
			else {
				float distChoiceCustomer = (float) Math.sqrt(
						Math.pow(choice.getOwner().getStandLatitude() - latitude, 2) + 
						Math.pow(choice.getOwner().getStandLongitude() - longitude, 2));
				float distDrinkCustomer = (float) Math.sqrt(
						Math.pow(selection.get(j).getOwner().getStandLatitude() - latitude, 2) + 
						Math.pow(selection.get(j).getOwner().getStandLongitude() - longitude, 2));
				if(distDrinkCustomer < distChoiceCustomer) {
					choice = selection.get(j);
				}
			}
		}
		return choice;
	}
	
	private Recipe getCheaper(List<Recipe> selection) {
		Recipe choice = null;
		for(int j=0; j<selection.size(); j++) {
			if(choice == null) {
				choice = selection.get(j);
			}
			else {
				if(selection.get(j).getPrice() < choice.getPrice()) {
					choice = selection.get(j);
				}
			}
		}
		return choice;
	}
	
	private Recipe selectDrink(Weather weather, List<Recipe> selection) {
		Recipe choice = null;
		if(weather == Weather.heatwave || weather == Weather.rainy) {
			choice = getNearest(selection);
		}
		else if(weather == Weather.sunny || weather == Weather.cloudy) {
			choice = getCheaper(selection);
		}
		return choice;
	}
	
	private List<Recipe> filterDrinks(long timestamp, Weather weather, List<Recipe> recipes) {
		boolean wantAlcohol = false;
		boolean wantCold = false;
		List<Recipe> selection = new ArrayList<Recipe>();
		if(timestamp%24 < 4 || timestamp%24 > 20) {
			wantAlcohol = true;
		}
		if(weather == Weather.heatwave || weather == Weather.sunny) {
			wantCold = true;
		}
		for(Recipe current : recipes) {
			if(current.hasAlcohol() == wantAlcohol && current.isCold() == wantCold) {
				selection.add(current);
			}
		}
		return selection;
	}
	
	private boolean wantToDrink(Weather weather) {
		boolean wantDrink = false;
		int random = (int) (Math.random() * 12);
		if(weather != Weather.thunder && !hasDrinkToday && random == 0) {
			wantDrink = true;
		}
		return wantDrink;
	}
	
	public Recipe chooseDrink(long timestamp, Weather weather, List<Recipe> recipes) {
		Recipe choice = null;
		if(wantToDrink(weather)) {
			List<Recipe> selection = filterDrinks(timestamp, weather, recipes);
			if(!selection.isEmpty()) {
				choice = selectDrink(weather, selection);
			}
		}
		return choice;
	}

	public void drink() {
		hasDrinkToday = true;
	}
	
	public float getLatitude() {
		return latitude;
	}
	
	public float getLongitude() {
		return longitude;
	}
}
