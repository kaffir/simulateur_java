
public class Recipe {
	private Player owner;
	private String name;
	private float price;
	private boolean hasAlcohol;
	private boolean isCold;
	
	public Recipe(Player player, String n, float p, boolean alcohol, boolean cold) {
		owner = player;
		name = n;
		price = p;
		hasAlcohol = alcohol;
		isCold = cold;
	}
	
	public String getName() {
		return name;
	}
	public float getPrice() {
		return price;
	}
	public boolean hasAlcohol() {
		return hasAlcohol;
	}
	public boolean isCold() {
		return isCold;
	}
	public Player getOwner() {
		return owner;
	}
}
