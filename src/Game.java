import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;

public class Game {
	private static Game instance = new Game();
	private static int numberOfCustomers = 50000;
	private long timestamp = 0;
	private Weather weather = Weather.sunny;
	private Map map;
	private Network net = new Network();
	private JSONFactory jf = new JSONFactory();
	private List<Player> players = new ArrayList<Player>();
	private List<Customer> customers = new ArrayList<Customer>();
	
	private Game() {}
	
	static public Game getInstance() {
		return instance;
	}
	
	public void reset() {
		try {
			net.reset();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void makeCustomers() {
		customers.clear();
		for(int i=0; i<numberOfCustomers; i++) {
			customers.add(new Customer(map.getLatitude(), map.getLatitudeSpan(), map.getLongitude(), map.getLongitudeSpan()));
			/*customers.add(new Customer(
			map.getLatitude()-map.getLatitudeSpan(), 
			map.getLatitude()+map.getLatitudeSpan(), 
			map.getLongitude()-map.getLongitudeSpan(), 
			map.getLongitude()+map.getLongitudeSpan()));*/
		}
	}
	
	private void drinksPurchases() {
		try {
			for(Customer current : customers) {
				List<MapItem> influences = map.compareWithMapItems(current.getLatitude(), current.getLongitude());
				if(!influences.isEmpty()) {
					List<Recipe> recipes = new ArrayList<Recipe>();
					for(MapItem inf : influences) {
						recipes.addAll(inf.getOwner().getRecipes());
					}
					Recipe customerChoice = current.chooseDrink(timestamp, weather, recipes);
					if(customerChoice != null) {
						JSONObject sales = jf.makeSales(customerChoice);
						if(net.postSales(sales)) {
							current.drink();
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void play() {
		long previous = -1;
		while(true) {
			try {
				JSONObject time = net.getTime();
				timestamp = jf.getTime(time);
				if(timestamp != previous) {
					JSONObject jsonMap = net.getMap();
					weather = jf.getWeather(time);
					players = jf.getPlayers(jsonMap);
					map = jf.getMap(jsonMap, players);
					if(timestamp/24 != previous/24 || previous == -1) {
						makeCustomers();
					}
					drinksPurchases();
				}
				previous = timestamp;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public List<Customer> getCustomers() {
		return customers;
	}
	
	public List<MapItem> getMapItems() {
		return map.getMapItems();
	}
	
	public List<Player> getPlayers() {
		return players;
	}
	
	public Weather getWeather() {
		return weather;
	}
	
	public long getTimestamp() {
		return timestamp;
	}
}
