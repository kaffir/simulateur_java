
public class MapItem {
	private MapItemKind kind;
	private Player owner;
	private float influence;
	private float latitude;
	private float longitude;
	
	public MapItem(MapItemKind k, Player p, float inf, float lat, float lon) {
		kind = k;
		owner = p;
		influence = inf;
		latitude = lat;
		longitude = lon;
	}

	public MapItemKind getKind() {
		return kind;
	}
	public Player getOwner() {
		return owner;
	}
	public float getInfluence() {
		return influence;
	}
	public float getLatitude() {
		return latitude;
	}
	public float getLongitude() {
		return longitude;
	}
}
