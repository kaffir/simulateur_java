import java.util.ArrayList;
import java.util.List;

import com.lynden.gmapsfx.GoogleMapView;
import com.lynden.gmapsfx.MapComponentInitializedListener;
import com.lynden.gmapsfx.javascript.object.GoogleMap;
import com.lynden.gmapsfx.javascript.object.LatLong;
import com.lynden.gmapsfx.javascript.object.MapOptions;
import com.lynden.gmapsfx.javascript.object.MapTypeIdEnum;
import com.lynden.gmapsfx.javascript.object.Marker;
import com.lynden.gmapsfx.javascript.object.MarkerOptions;

import javafx.application.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.stage.*;
import javafx.scene.* ;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Paint;
import javafx.scene.text.Text;

public class HMI extends Application implements MapComponentInitializedListener {
	Game game = Game.getInstance();
	BorderPane root;
	GoogleMapView mapView;
	GoogleMap map;
	Button reset;
	Text dateHour = new Text();
	Text weather = new Text();
	List<Marker> markers = new ArrayList<>();
    ListView<String> ranking = new ListView<String>();
	
	final Service<Void> playGame = new Service<Void>() {
		@Override
		protected Task<Void> createTask() {
			return new Task<Void>() {
				@Override
				protected Void call() throws Exception {
					game.play();
					return null;
				}
			};
		}
	};
    
	final Service<Void> refreshPrint = new Service<Void>() {
		@Override
		protected Task<Void> createTask() {
			return new Task<Void>() {
				@Override
				protected Void call() throws Exception {
					while(true) {
						List<Player> players = game.getPlayers();
						ObservableList<String> items = FXCollections.observableArrayList();
						for(Player current : players) {
							items.add(current.getName() + " : " + current.getCash() + " €");
						}
						ranking.setItems(items);
						long timestamp = game.getTimestamp();
						dateHour.setText("Day : " + timestamp/24 + " Hour : " + timestamp%24);
						weather.setText(game.getWeather().toString().toUpperCase());
						Thread.sleep(10000);
					}
				}
			};
		}
	};
	
	/*final Service<Void> sleepMap = new Service<Void>() {
		@Override
		protected Task<Void> createTask() {
			return new Task<Void>() {
				@Override
				protected Void call() throws Exception {
					Thread.sleep(10000);
					return null;
				}
			};
		}
	};*/
	
	@Override
	public void mapInitialized() {
	    MapOptions mapOptions = new MapOptions();
	    mapOptions.center(new LatLong(0, 0))
	            .mapType(MapTypeIdEnum.ROADMAP)
	            .overviewMapControl(true)
	            .panControl(false)
	            .rotateControl(false)
	            .scaleControl(false)
	            .streetViewControl(false)
	            .zoomControl(true)
	            .zoom(12);
	    map = mapView.createMap(mapOptions);
	    /*List<Player> players = game.getPlayers();
		for(Player current : players) {
		    MarkerOptions markerOptions = new MarkerOptions();
			markerOptions.position( new LatLong(current.getStandLatitude(), current.getStandLongitude()) );
			markerOptions.visible(Boolean.TRUE);
			markerOptions.title("Customer");
		    Marker marker = new Marker( markerOptions );
		    markers.add(marker);
		}*/
	}
	
	private void setMap() {
	    mapView = new GoogleMapView();
	    mapView.addMapInializedListener(this);
	    mapView.cursorProperty();
	    root.setCenter(mapView);
	}
	
	private void setButtonReset() {
	    reset = new Button("Nouvelle Partie");
	    reset.setMnemonicParsing(false);
	    reset.setOnAction(new EventHandler<ActionEvent>() {
	        @Override public void handle(ActionEvent e) {
	            game.reset();
	            Alert alert = new Alert(AlertType.INFORMATION);
				alert.setHeaderText("Nouvelle partie :");
	            alert.setContentText("La partie a été (ré)initialisée");
	            alert.showAndWait();
	        }
	    });
	    BorderPane buttonPane = new BorderPane();
	    buttonPane.setPadding(new Insets(10, 10, 10, 10));
	    buttonPane.setCenter(reset);
	    root.setBottom(buttonPane);
	}
	
	private void setRankPane() {
	    BorderPane labelPane = new BorderPane();
	    labelPane.setPadding(new Insets(10, 10, 10, 10));
	    labelPane.setCenter(new Text("Classement des joueurs par budget"));
	    BorderPane rankPane = new BorderPane();
	    rankPane.setPadding(new Insets(10, 10, 10, 10));
	    rankPane.setTop(labelPane);
	    rankPane.setCenter(ranking);
	    root.setRight(rankPane);
	}
	
	private void setTopPane() {
	    BorderPane topPane = new BorderPane();
	    topPane.setPadding(new Insets(10, 10, 10, 10));
	    topPane.setLeft(dateHour);
	    topPane.setRight(weather);
	    root.setTop(topPane);
	}
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		primaryStage.setTitle("ULTRA STREET LEMON SALES MULTIPLAYER EDITION FEATURING KNUCKLES");
		primaryStage.setWidth(800);
		primaryStage.setHeight(600);
	    root = new BorderPane();
	    root.setPadding(new Insets(10, 10, 10, 10));
	    root.setBackground(new Background(new BackgroundFill(Paint.valueOf("gold"), new CornerRadii(0), new Insets(0))));
	    setMap();
	    setTopPane();
	    setRankPane();
	    setButtonReset();
	    playGame.start();
	    refreshPrint.start();
		Scene scene = new Scene(root);
		primaryStage.setScene(scene);
		primaryStage.show();
	}

	public static void main(String[] args) {
		HMI.launch(args);
	}
}